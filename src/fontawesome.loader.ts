import fontawesome from '@fortawesome/fontawesome';
import * as faTwitter from '@fortawesome/fontawesome-free-brands/faTwitter';
import * as faLinkedin from '@fortawesome/fontawesome-free-brands/faLinkedin';
import * as faSlack from '@fortawesome/fontawesome-free-brands/faSlack';
import * as faGitlab from '@fortawesome/fontawesome-free-brands/faGitlab';
import * as faCode from '@fortawesome/fontawesome-pro-regular/faCode';

export function loadFontAwesomeIcons() {

    fontawesome.library.add(faTwitter, faLinkedin, faSlack, faGitlab, faCode);
}
